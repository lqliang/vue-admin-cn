---
date: 2020-11-17
title: 环境配置
describe: 环境配置
---

# 环境配置

本章节将对项目基础环境做简单说明。


## 前端

1. 先安装 node 版本管理工具，然后根据版本管理工具安装 node

  - 安装 [fnm](https://github.com/Schniz/fnm)
  - 安装 [nvm](https://github.com/nvm-sh/nvm)

2. 也可直接前往[https://nodejs.org/zh-cn/](https://nodejs.org/zh-cn/)下载当前版本node进行安装
3. 然后在命令行运行 ```node -v``` 若输出版本号则 node 安装成功
4. 开发工具推荐vscode [https://code.visualstudio.com/](https://code.visualstudio.com/)

## 后端

1. [Nestjs](https://nestjs.com/)
2. [Express](https://expressjs.com/)
3. [Koa2](https://koajs.com/)
